# Summary
![TestSummary](/uploads/5ec4df0e7292f97661c6be10e835c560/TestSummary.JPG)

![TestSummary2](/uploads/07efb2a3a9d0c6963d9a60d8150094e6/TestSummary2.JPG)

* OP-T13 "Purchase successful, Download" test was omitted in test execution due to my budgetary restraints

## LOGIN: 6 Test Cases
![Login](/uploads/fc51b7374f4dc9f931b4f79999b3f0b9/Testcaseoverview1.JPG)

## SIGNUP: 4 Test cases
![Signup](/uploads/5b5f6e692b9ff93a853ebecf240e476a/TestCaseSignup.JPG)

## INTEGRATIONS: 3 Test cases
![Integrations](/uploads/d7d046a6739aa701d75fbe04727bb812/TestIntegrations.JPG)

### Test Detail Example
![Testcasedetail](/uploads/21f51147a0b0c175e62209fe60d68b3f/Testcaseover2.JPG)

### Test Script Example
![Testscript](/uploads/76cb1704141ecd4cc0adebf3d7369511/Testcaseover3.JPG)
