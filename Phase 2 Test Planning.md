# Scope
* Login
* Signup
* Integrations

# Test Environments
### Operating system
* Windows 10

### Browser
* Google Chrome 114

# Entry/Exit Criteria
## Requirement Analysis Stage
* Entry: Test team receives requirements
* Exit: Requirements are explored and understood

## Test Execution Stage
* Entry: Application is ready for testing
* Exit: Test case reports and defect reports are ready

## Test Closing Stage
* Entry: Test case reports and defect reports are ready
* Exit: Test summary reports are ready

# Tools
* JIRA and Zephyr Scale
* Snipping Tool
* Excel
* VSCode
