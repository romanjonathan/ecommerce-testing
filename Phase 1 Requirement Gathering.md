# Requirements
### LOGIN:
* Description: User is able to create an account and log in
* Priority: High
* Acceptance Criteria: The user can reach the log in page from the home page,
enter their credentials, and click the log in button.
User is then redirected to their account dashboard after logging in.

### SIGNUP:
* Description: User is able to register for an account
* Priority: High
* Acceptance Criteria: User can reach the registration page from the home page,
enter their details, and click the register button. User is then redirected
to their account dashboard.

### INTEGRATIONS:
* Description: User is able to download integrations
* Priority: Medium
* Acceptance Criteria: User can download integrations from the marketplace.


# User Stories
### LOGIN:
* As a user, I want to be able to log in to my account in order to purchase items.

### SIGNUP:
* As a new user, I want to be able to create an account in order to use the site.

### INTEGRATIONS:
* As a user, I want to be able to download integrations for my ecommerce site.


# Sample Design Mockups
### LOGIN:
![Opencartlogin](/uploads/935b631752eb07f58c7322de92f6315a/Opencartlogin.JPG)

### SIGNUP:
![Opencartregister](/uploads/8c88ee5a8ade549930e1ae586f2082f3/Opencartregister.JPG)

### INTEGRATIONS:
![Integrations](/uploads/5400edfe90f1499fc5881567f63bb13f/integrations.JPG)
